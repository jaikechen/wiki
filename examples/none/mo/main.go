package main

import (
	"encoding/json"
	"fmt"
	"github.com/samber/mo"
)

type User struct {
	Firstname mo.Option[string] `json:"firstname"`
	Lastname  mo.Option[string] `json:"lastname"`
	Age       mo.Option[int]    `json:"age"`
	IsVip     mo.Option[bool]   `json:"isVip"`
}

var defaultUser = User{
	Firstname: mo.Some("Joe"),
	Lastname:  mo.Some("Doe"),
	Age:       mo.Some(30),
	IsVip:     mo.Some(true),
}

func updateUser(payload string) error {
	var inputUser User
	if err := json.Unmarshal([]byte(payload), &inputUser); err != nil {
		return err
	}
	if inputUser.IsVip.IsPresent() {
		defaultUser.IsVip = inputUser.IsVip
	}
	if inputUser.Firstname.IsPresent() {
		defaultUser.Firstname = inputUser.Firstname
	}
	if inputUser.Lastname.IsPresent() {
		defaultUser.Lastname = inputUser.Lastname
	}
	if inputUser.Age.IsPresent() {
		defaultUser.Age = inputUser.Age
	}
	return nil
}

func main() {
	fmt.Println("before update age:", defaultUser.Firstname, defaultUser.Lastname, defaultUser.IsVip, defaultUser.Age)
	if err := updateUser(`{"age":40}`); err != nil {
		fmt.Println(err)
	}
	fmt.Println("after update age:", defaultUser.Firstname, defaultUser.Lastname, defaultUser.IsVip, defaultUser.Age)

	if bs, err := json.Marshal(defaultUser); err == nil {
		fmt.Println("json payload of default: ", string(bs))
	}
}
