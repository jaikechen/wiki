package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Firstname *string `json:"firstname"`
	Lastname  *string `json:"lastname"`
	Age       *int    `json:"age"`
	IsVip     *bool   `json:"isVip"`
}

func ptr[T any](v T) *T {
	return &v
}

var defaultUser = User{
	Firstname: ptr("Joe"),
	Lastname:  ptr("Doe"),
	Age:       ptr(30),
	IsVip:     ptr(true),
}

func updateUser(payload string) error {
	var inputUser User
	if err := json.Unmarshal([]byte(payload), &inputUser); err != nil {
		return err
	}
	if inputUser.IsVip != nil {
		defaultUser.IsVip = inputUser.IsVip
	}
	if inputUser.Firstname != nil {
		defaultUser.Firstname = inputUser.Firstname
	}
	if inputUser.Lastname != nil {
		defaultUser.Lastname = inputUser.Lastname
	}
	if inputUser.Age != nil {
		defaultUser.Age = inputUser.Age
	}
	return nil
}

func main() {
	fmt.Println("before update age:", *defaultUser.Firstname, *defaultUser.Lastname, *defaultUser.IsVip, *defaultUser.Age)
	if err := updateUser(`{"age":40}`); err != nil {
		fmt.Println(err)
	}
	fmt.Println("after update age:", *defaultUser.Firstname, *defaultUser.Lastname, *defaultUser.IsVip, *defaultUser.Age)
}
