package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Age       int    `json:"age"`
	IsVip     bool   `json:"isVip"`
}

var defaultUser = User{
	Firstname: "Joe",
	Lastname:  "Doe",
	Age:       30,
	IsVip:     true,
}

func updateUser(payload string) error {
	var inputUser User
	if err := json.Unmarshal([]byte(payload), &inputUser); err != nil {
		return err
	}
	defaultUser.IsVip = inputUser.IsVip
	defaultUser.Firstname = inputUser.Firstname
	defaultUser.Lastname = inputUser.Lastname
	defaultUser.Age = inputUser.Age
	return nil
}

func main() {
	fmt.Println(defaultUser)
	if err := updateUser(`{"age":40}`); err != nil {
		fmt.Println(err)
	}
	fmt.Println(defaultUser)
}
