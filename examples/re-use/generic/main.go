package main

import (
	"fmt"
	"time"
)

func Ptr[T any](t T) *T {
	return &t
}

func main() {
	//no generic
	ts := time.Now()
	timePtr := &ts

	//with generic
	timePtr1 := Ptr(time.Now())

	fmt.Println(*timePtr, *timePtr1)
}
