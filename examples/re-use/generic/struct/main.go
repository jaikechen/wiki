package main

import (
	"fmt"
	"golang.org/x/exp/constraints"
	"sync"
	"time"
)

type TTLEntity[T any] struct {
	Item T
	TS   time.Time
}

type TTLCache[K constraints.Ordered, T any] struct {
	cache    sync.Map
	duration time.Duration
}

func NewTTLCache[K constraints.Ordered, T any](duration time.Duration) *TTLCache[K, T] {
	return &TTLCache[K, T]{
		cache:    sync.Map{},
		duration: duration,
	}
}

func (c *TTLCache[K, T]) Set(k K, v T) {
	entity := TTLEntity[T]{
		Item: v,
		TS:   time.Now(),
	}
	c.cache.Swap(k, entity)
}

func (c *TTLCache[K, T]) Get(k K) (T, bool) {
	var empty T
	item, ok := c.cache.Load(k)
	if !ok {
		return empty, false
	}

	ttlEntity := item.(TTLEntity[T])
	if ttlEntity.TS.Add(c.duration).Before(time.Now()) {
		return empty, false
	}
	return ttlEntity.Item, true
}

type Post struct {
	ID    int
	Title string
}

func main() {
	store := NewTTLCache[int, Post](time.Second * 10)
	p := Post{
		ID:    1,
		Title: "post one",
	}

	store.Set(p.ID, p)
	fmt.Println(store.Get(p.ID))
	time.Sleep(time.Second * 11)
	fmt.Println(store.Get(p.ID))

}
