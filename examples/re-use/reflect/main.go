package main

import (
	"fmt"
	"reflect"
)

func MergeDefaults[T any](defaultItem, newItem T) T {
	defaultValues := reflect.ValueOf(&defaultItem).Elem()

	types := reflect.TypeOf(newItem)
	for i := 0; i < types.NumField(); i++ {
		newValue := reflect.ValueOf(newItem).Field(i)
		empty := reflect.Zero(types.Field(i).Type).Interface()

		if !reflect.DeepEqual(newValue.Interface(), empty) {
			targetValue := defaultValues.Field(i)
			targetValue.Set(newValue)
		}
	}
	return defaultItem
}

type DbConnectionConfig struct {
	Host              string
	DB                string
	Username          string
	Password          string
	MaxConnection     int
	MaxIdleConnection int
}

func main() {
	defaultConfig := DbConnectionConfig{
		MaxConnection:     100,
		MaxIdleConnection: 10,
	}
	config := MergeDefaults(defaultConfig, DbConnectionConfig{
		Host:     "127.0.0.1",
		DB:       "test",
		Username: "test",
		Password: "test",
	})
	fmt.Println(defaultConfig)
	fmt.Println(config)
}
