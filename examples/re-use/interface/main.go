package main

import (
	"fmt"
	"sort"
)

type BaseEntity struct {
	ID int
}

func (b BaseEntity) GetID() int {
	return b.ID
}

type Post struct {
	BaseEntity
	Title string
}

type User struct {
	BaseEntity
	Name string
}

type IDable interface {
	GetID() int
}

func Sort(entities []IDable) {
	sort.Slice(entities, func(i, j int) bool {
		return entities[i].GetID() < entities[j].GetID()
	})
}

func main() {
	var posts []IDable
	posts = append(posts, Post{
		Title:      "first post",
		BaseEntity: BaseEntity{ID: 2},
	})
	posts = append(posts, Post{
		Title:      "second post",
		BaseEntity: BaseEntity{ID: 1},
	})
	fmt.Println("Before sort:", posts)
	Sort(posts)
	fmt.Println("After sort", posts)
}
