package main

import "sort"

type Post struct {
	ID    int
	Title string
}

func SortPost(entities []Post) {
	sort.Slice(entities, func(i, j int) bool {
		return entities[i].ID < entities[j].ID
	})
}

type User struct {
	ID   int
	Name string
}

func SortUser(entities []User) {
	sort.Slice(entities, func(i, j int) bool {
		return entities[i].ID < entities[j].ID
	})
}
