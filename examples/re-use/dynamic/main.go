package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func GetAPIResult[T any](url string) (T, error) {
	var empty T
	// Send HTTP GET request to the API endpoint

	response, err := http.Get(url)
	if err != nil {
		return empty, err
	}
	defer response.Body.Close()

	// Check if the response status code is not OK (200)
	if response.StatusCode != http.StatusOK {
		return empty, fmt.Errorf("HTTP request failed with status code %d", response.StatusCode)
	}

	// Parse the JSON response body into a slice of map[string]interface{}
	var results T
	err = json.NewDecoder(response.Body).Decode(&results)
	if err != nil {
		return empty, err
	}

	return results, nil
}

func main() {
	items, err := mergedItems(
		"https://jsonplaceholder.typicode.com/posts",
		"https://jsonplaceholder.typicode.com/users/%v",
		"userId",
		"user")
	if err != nil {
		panic(err)
	}
	if bs, err := json.Marshal(items); err != nil {
		panic(err)
	} else {
		fmt.Println(string(bs))
	}
}

type Item map[string]any

func mergedItems(mainEntityURL, lookupEntityURL, localIDField, lookupField string) ([]Item, error) {
	items, err := GetAPIResult[[]Item](mainEntityURL)
	if err != nil {
		return nil, err
	}
	for _, item := range items {
		localID := fmt.Sprintf("%v", item[localIDField])
		url := fmt.Sprintf(lookupEntityURL, localID)
		lookupEntity, err := GetAPIResult[Item](url)
		if err != nil {
			return nil, err
		}
		item[localIDField] = lookupEntity
	}
	return items, nil
}
