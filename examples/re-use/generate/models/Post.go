package models

import (
	"context"
	"database/sql"
)

type Post struct {
	userId float64
	id     float64
	title  string
	body   string
}

func (e Post) Insert(ctx context.Context, db *sql.DB) (int64, error) {
	result, err := db.ExecContext(ctx,
		"insert into Post (userId,id,title,body) values(?,?,?,?)",
		[]any{e.userId, e.id, e.title, e.body})
	if err != nil {
		return 0, err
	}
	return result.LastInsertId()
}
