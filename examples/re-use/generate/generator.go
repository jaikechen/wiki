package generate

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"reflect"
	"strings"
)

var ormTemplate = `package models
import (
	"context"
	"database/sql"
)

type [entityName] struct{
[structFields]
}

func (e Post) Insert(ctx context.Context, db *sql.DB) (int64, error) {
	result, err := db.ExecContext(ctx, 
		"insert into Post ([insertFields]) values([insertPlaceHolders])", 
		[]any{[insertParams]})
	if err != nil {
		return 0, err
	}
	return result.LastInsertId()
}
`

type Item map[string]any

func GenerateORM(item Item, entityName string) error {
	structFields, insertFields, insertParams, insertPlaceHolders := "", "", "", ""
	for s, a := range item {
		structFields += fmt.Sprintf("\n	%s %s", s, reflect.TypeOf(a).Name())
		insertFields += fmt.Sprintf(",%s", s)
		insertParams += fmt.Sprintf(",e.%s", s)
		insertPlaceHolders += ",?"
	}

	code := strings.ReplaceAll(ormTemplate, "[entityName]", entityName)
	code = strings.ReplaceAll(code, "[structFields]", structFields[1:])
	code = strings.ReplaceAll(code, "[insertFields]", insertFields[1:])
	code = strings.ReplaceAll(code, "[insertPlaceHolders]", insertPlaceHolders[1:])
	code = strings.ReplaceAll(code, "[insertParams]", insertParams[1:])

	fileName := fmt.Sprintf("./models/%s.go", entityName)
	return os.WriteFile(fileName, []byte(code), 0644)
}

func GetAPIResult[T any](url string) (T, error) {
	var empty T
	// Send HTTP GET request to the API endpoint

	response, err := http.Get(url)
	if err != nil {
		return empty, err
	}
	defer response.Body.Close()

	// Check if the response status code is not OK (200)
	if response.StatusCode != http.StatusOK {
		return empty, fmt.Errorf("HTTP request failed with status code %d", response.StatusCode)
	}

	// Parse the JSON response body into a slice of map[string]interface{}
	var results T
	err = json.NewDecoder(response.Body).Decode(&results)
	if err != nil {
		return empty, err
	}

	return results, nil
}
