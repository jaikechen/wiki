package generate

import "testing"

func TestGenerateModel(t *testing.T) {
	item, err := GetAPIResult[Item]("https://jsonplaceholder.typicode.com/posts/1")
	if err != nil {
		panic(err)
	}
	if err := GenerateORM(item, "Post"); err != nil {
		panic(err)
	}
}
