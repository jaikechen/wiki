package main

import (
	"fmt"
	"io"
	"os"
	"path"
	"runtime"
)

func main() {
	logLevel := "debug"
	if err := backup("a"); err != nil {
		if logLevel == "debug" {
			fmt.Println(err)
		}
	}
}

func annotate(err error) error {
	pc, file, line, _ := runtime.Caller(1)
	_, fileName := path.Split(file)
	callerFunc := runtime.FuncForPC(pc)
	return fmt.Errorf("%s:%d[%s] %w", fileName, line, callerFunc.Name(), err)
}

func backup(file string) error {
	if err := copy(file, file+".back"); err != nil {
		return annotate(err)
	}
	return nil
}

func copy(src, target string) error {
	srcFile, err := os.Open(src)
	if err != nil {
		return annotate(err)
	}
	defer srcFile.Close()

	targetFile, err := os.Create(target)
	if err != nil {
		return annotate(err)
	}
	defer targetFile.Close()

	_, err = io.Copy(targetFile, srcFile)
	if err != nil {
		return annotate(err)
	}
	return nil
}

func copy1(src, target string) (bool, error) {
	// Open the source file for reading
	srcFile, err := os.Open(src)
	if err != nil {
		return false, err
	}
	defer srcFile.Close()

	// Create or truncate the target file for writing
	targetFile, err := os.Create(target)
	if err != nil {
		fmt.Println(err)
		return false, err
	}
	defer targetFile.Close()

	// Copy the content from source to target
	_, err = io.Copy(targetFile, srcFile)
	if err != nil {
		fmt.Println(err)
		return false, err
	}
	return true, nil
}
