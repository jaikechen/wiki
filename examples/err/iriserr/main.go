package main

import (
	"fmt"
	"github.com/kataras/iris/v12"
	"path"
	"runtime"
)

const Debug = true

func errorHandler(ctx iris.Context) {
	if err := ctx.GetErr(); err != nil {
		if Debug {
			ctx.StopWithError(500, err)
		} else {
			ctx.SetErr(nil)
			ctx.StopWithStatus(500)
		}
		fmt.Println(err)
	}
}

func annotateError(msg string) error {
	pc, file, line, _ := runtime.Caller(1)
	_, fileName := path.Split(file)
	callerFunc := runtime.FuncForPC(pc)
	return fmt.Errorf("%s:%d[%s] %s", fileName, line, callerFunc.Name(), msg)
}

func main() {
	app := iris.New()

	app.Get("/", func(ctx iris.Context) {
		ctx.SetErr(annotateError("home page error"))
		ctx.Next()
	}, errorHandler)

	app.Get("/b", func(ctx iris.Context) {
		ctx.SetErr(annotateError("demo error"))
		ctx.Next()
	}, errorHandler)

	app.Run(iris.Addr(":8080"))
}
