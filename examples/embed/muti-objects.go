package main

import (
	"encoding/json"
	"fmt"
	"github.com/samber/lo"
)

type Circle struct {
	Type   string `json:"type"`
	Radius int    `json:"radius"`
}

type Rectangle struct {
	Type   string `json:"type"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
}

type Object struct {
	Circle
	Rectangle
}

func main() {
	jsonData := []byte(`[
            {
                "radius": 5
            },
            {
                "width": 10,
                "height": 20
            }
        ]
    `)

	var result []Object
	if err := json.Unmarshal(jsonData, &result); err != nil {
		fmt.Println("Error:", err)
		return
	}
	for _, object := range result {
		switch {
		case !lo.IsEmpty(object.Circle):
			fmt.Println("circle radius is:", object.Circle.Radius)
		case !lo.IsEmpty(object.Rectangle):
			fmt.Println("rectangle height is: ", object.Rectangle.Height)
			fmt.Println("rectangle width is: ", object.Rectangle.Width)
		}
	}
}
