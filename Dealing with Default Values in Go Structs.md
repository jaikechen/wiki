# Dealing with Default Values in Go Structs

When working with RESTful APIs, handling JSON payloads is a common task. However, a significant issue arises when a 
frontend sends partial data updates. This scenario can lead to ambiguity for backend developers regarding which fields 
were intentionally left unchanged. In this blog post, we explore the challenges posed by Go's default values for struct 
fields and propose solutions to mitigate this issue.

## issue of default value of go struct
An instance of a RESTful API use case often involves the frontend passing a JSON string 
to the `updateUser` function. To facilitate this scenario, the following code is crafted:
```
package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Age       int    `json:"age"`
	IsVip     bool   `json:"isVip"`
}

var defaultUser = User{
	Firstname: "Joe",
	Lastname:  "Doe",
	Age:       30,
	IsVip:     true,
}

func updateUser(payload string) error {
	var inputUser User
	if err := json.Unmarshal([]byte(payload), &inputUser); err != nil {
		return err
	}
	defaultUser.IsVip = inputUser.IsVip
	defaultUser.Firstname = inputUser.Firstname
	defaultUser.Lastname = inputUser.Lastname
	defaultUser.Age = inputUser.Age
	return nil
}

func main() {
	fmt.Println(defaultUser)
	if err := updateUser(`{"age":40}`); err != nil {
		fmt.Println(err)
	}
	fmt.Println(defaultUser)
}
```
In the provided code, the frontend developer assumes that providing a payload of 
`{"age":40}` is sufficient since only the age has been changed in the web form. 
However, for the backend developer, this presents a challenge. 
When Go unmarshals `{"age":40}`, it assigns default values to all fields, 
making it impossible to differentiate between a payload of `{"age":40}` and 
`{"age":40, "isVip": false}`.

To address this issue, we explore alternative ways to represent 'None' in Go structs.
## use pointer
Here's the pointer version of the code:

```
package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Firstname *string `json:"firstname"`
	Lastname  *string `json:"lastname"`
	Age       *int    `json:"age"`
	IsVip     *bool   `json:"isVip"`
}

func ptr[T any](v T) *T {
	return &v
}

var defaultUser = User{
	Firstname: ptr("Joe"),
	Lastname:  ptr("Doe"),
	Age:       ptr(30),
	IsVip:     ptr(true),
}

func updateUser(payload string) error {
	var inputUser User
	if err := json.Unmarshal([]byte(payload), &inputUser); err != nil {
		return err
	}
	if inputUser.IsVip != nil {
		defaultUser.IsVip = inputUser.IsVip
	}
	if inputUser.Firstname != nil {
		defaultUser.Firstname = inputUser.Firstname
	}
	if inputUser.Lastname != nil {
		defaultUser.Lastname = inputUser.Lastname
	}
	if inputUser.Age != nil {
		defaultUser.Age = inputUser.Age
	}
	return nil
}

func main() {
	fmt.Println(*defaultUser.Firstname, *defaultUser.Lastname, *defaultUser.IsVip, *defaultUser.Age)
	if err := updateUser(`{"age":40}`); err != nil {
		fmt.Println(err)
	}
	fmt.Println(*defaultUser.Firstname, *defaultUser.Lastname, *defaultUser.IsVip, *defaultUser.Age)
}

```
If the frontend only passes the age in the JSON payload, then only the age field of 
the defaultUser struct will be updated, leaving the other fields unchanged. 
This ensures that the backend can accurately interpret and apply the changes 
specified by the frontend.
```
before update age: Joe Doe true 30
after update age: Joe Doe true 40
```
While using pointers in a struct can accomplish the desired outcome of discerning which fields were passed by the JSON frontend, it comes with its drawbacks:

1. **Implicit Implication of Field Absence**: Determining whether a field is absent requires checking if it's nil throughout the codebase, leading to increased complexity and reduced readability.

2. **Potential for Nil Pointer Errors**: The use of pointers introduces the risk of encountering nil pointer errors, which can occur when accessing fields that have not been properly initialized.

To address these challenges, the "Monad Go"(github.com/samber/mo) package offers a more elegant solution for handling optional values in Go structs.

## use mo.Option
The following iteration of the code leverages mo.None from the "Monad Go" package:
```
package main

import (
	"encoding/json"
	"fmt"
	"github.com/samber/mo"
)

type User struct {
	Firstname mo.Option[string] `json:"firstname"`
	Lastname  mo.Option[string] `json:"lastname"`
	Age       mo.Option[int]    `json:"age"`
	IsVip     mo.Option[bool]   `json:"isVip"`
}

var defaultUser = User{
	Firstname: mo.Some("Joe"),
	Lastname:  mo.Some("Doe"),
	Age:       mo.Some(30),
	IsVip:     mo.Some(true),
}

func updateUser(payload string) error {
	var inputUser User
	if err := json.Unmarshal([]byte(payload), &inputUser); err != nil {
		return err
	}
	if inputUser.IsVip.IsPresent() {
		defaultUser.IsVip = inputUser.IsVip
	}
	if inputUser.Firstname.IsPresent() {
		defaultUser.Firstname = inputUser.Firstname
	}
	if inputUser.Lastname.IsPresent() {
		defaultUser.Lastname = inputUser.Lastname
	}
	if inputUser.Age.IsPresent() {
		defaultUser.Age = inputUser.Age
	}
	return nil
}

func main() {
	fmt.Println("before update age:", defaultUser.Firstname, defaultUser.Lastname, defaultUser.IsVip, defaultUser.Age)
	if err := updateUser(`{"age":40}`); err != nil {
		fmt.Println(err)
	}
	fmt.Println("after update age:", defaultUser.Firstname, defaultUser.Lastname, defaultUser.IsVip, defaultUser.Age)

	if bs, err := json.Marshal(defaultUser); err == nil {
		fmt.Println("json payload of default: ", string(bs))
	}
}
```
Here, mo.None from the "Monad Go" package is used to signify the absence of a value in 
optional fields within the User struct. This enhances the clarity and robustness of the 
code, making it easier to handle optional fields while improving readability and maintainability.
