#  Unlocking Go's Power: Exploring Struct Embedding for Composition and Code Reuse
In the realm of Go programming, struct embedding stands out as a powerful feature that 
facilitates code composition and reuse. By embedding one struct within another, 
developers can harness the combined capabilities of multiple structs, 
leading to cleaner, more modular code.  

This article delves into the versatility of struct embedding in Go, 
highlighting three key usage scenarios where it shines brightest. 
## Usage One: Implement default interface
When embeds a struct, the new struct not only embeds it's field, but also embeds it's methods.
While I was trying to encapsulate some common Redis Cache Operation, I added two structs

```go
package redis

import "context"

type Zset struct {
}

func (z *Zset) Add(ctx context.Context, key string, score int64) error {
}

func (z *Zset) Delete(ctx context.Context, key string) error{
}

type Str struct{
}
func (s *Str) Add(ctx context.Context, key string, val string) error{
}

func (z *Zset) Delete(ctx context.Context, key string) error{
}
```
While in redis delete a zset value and str value using the same command, we don't need
implement separately for both Zset and Str, the refactored code looks like below
```go
package redis

import "context"

type Deletable struct{
}

func (d Deletable) Delete(ctx context.Context, key string)  error{
}

type Zset struct {
	*Deletable
}

func (z *Zset) Add(ctx context.Context, key string, score int64) {
}

type Str struct{
	*Deletable
}
func (s *Str) Add(ctx context.Context, key string, val string){
}

```
The embed feature are pretty powerful, we can embed multiple struct, this way we kind of 
have c++'s multi-inherent ability while the implementation looks simple and straightforward. 

## Usage Two: unmarshal mix Objects
When communicate with external system, we use json as the serialize/un-serialize object.
Sometimes we have to handle mixed objects, Here's a simple example where we have JSON data with mixed objects:

```json
{
  "objects": [
    {
      "radius": 5
    },
    {
      "width": 10,
      "height": 20
    }
  ]
}
```
We have two types of objects here: circle and rectangle.

Here's how you can unmarshal this JSON data into Go structs:

```go
package main

import (
	"encoding/json"
	"fmt"
	"github.com/samber/lo"
)

type Circle struct {
	Type   string `json:"type"`
	Radius int    `json:"radius"`
}

type Rectangle struct {
	Type   string `json:"type"`
	Width  int    `json:"width"`
	Height int    `json:"height"`
}

type Object struct {
	Circle
	Rectangle
}

func main() {
	jsonData := []byte(`[
            {
                "radius": 5
            },
            {
                "width": 10,
                "height": 20
            }
        ]
    `)

	var result []Object
	if err := json.Unmarshal(jsonData, &result); err != nil {
		fmt.Println("Error:", err)
		return
	}
	for _, object := range result {
		switch {
		case !lo.IsEmpty(object.Circle):
			fmt.Println("circle radius is:", object.Circle.Radius)
		case !lo.IsEmpty(object.Rectangle):
			fmt.Println("rectangle height is: ", object.Rectangle.Height)
			fmt.Println("rectangle width is: ", object.Rectangle.Width)
		}
	}
}
```
In this example, we use embed struct to make unmarshal code clean 

## In conclusion
struct embedding in Go offers a potent mechanism for enhancing code organization and promoting reuse. By embedding one struct within another, developers can leverage the combined functionalities of multiple structs, leading to more modular and maintainable codebases.
This article has explored the versatility of struct embedding through three key usage scenarios:

- Implementing default interfaces: Struct embedding allows for the consolidation of common methods and behaviors, streamlining code and reducing redundancy. By embedding structs with shared functionalities and methods into more specialized structs, developers can simplify implementation and maintenance tasks, as demonstrated in the example of encapsulating Redis cache operations.

- Unmarshaling mixed objects: Struct embedding facilitates the handling of mixed objects during JSON serialization and deserialization. By embedding structs representing different object types into a single struct, developers can unmarshal JSON data seamlessly, simplifying code logic and improving readability. The provided example showcased how struct embedding enables straightforward unmarshaling of mixed JSON objects representing circles and rectangles.

In both scenarios, struct embedding empowers developers to create cleaner, more expressive code while promoting code reuse and modularity. By strategically employing struct embedding, Go programmers can unlock the language's full potential and build robust, maintainable applications with ease.





